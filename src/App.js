import Blog from "./pages/Blog";
import "./App.css";

function App() {
  return (
    <div>
      <Blog />
    </div>
  );
}

export default App;
