const BlogItems = ({data}) => {
    console.log(data)
    return (
        <div className="basis-1/3 max-w-[370px]">
            <img src={data.image} className="w-[370px] h-[220px] rounded-[5px]"/>
            <p className="text-white text-[12px] leading-6 font-semibold bg-[#3056D3] w-[115.868px] rounded-[5px] text-center mt-[30px]">{data.time}</p>
            <p className="mt-[21.84px] text-[#212B36] text-[24px] font-semibold leading-8">{data.title}</p>
            <p className="mt-[15px] text-[16px] leading-7 font-normal">{data.description}</p>
        </div>
    )
}

export default BlogItems;