import blog_image_1 from "./assets/images/blog-image.jpg";
import blog_image_2 from "./assets/images/blog-image-2.jpg";
import blog_image_3 from "./assets/images/blog-image-3.jpg";

const items = [
    {
        image: blog_image_1,
        time: "Dec 22, 2023",
        title: "Meet AutoManage, the best AI management tools",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
        image: blog_image_2,
        time: "Mar 15, 2023",
        title: "How to earn more money as a wellness coach",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },
    {
        image: blog_image_3,
        time: "Jan 05, 2023",
        title: "The no-fuss guide to upselling and cross selling",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    }
];

export default items;