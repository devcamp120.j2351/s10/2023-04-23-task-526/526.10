import BlogItems from "../components/BlogItems";
import data from "../data";

const Blog = () => {
    console.log(data);
    return (
        <div>
            <div className="text-[#3056D3] text-center text-[18px] font-semibold leading-6 mt-[120px]">Our Blogs</div>
            <div className="text-[#2E2E2E] text-center text-[40px] font-bold leading-[45px] capitalize mt-[8px]">Our Recent News</div>
            <div className="text-[#637381] text-center text-[15px] font-normal leading-[25px] mt-[15px] mb-[80px]">There are many variations of passages of Lorem Ipsum available<br/>but the majority have suffered alteration in some form.</div>
            <div className="flex flex-row max-w-all mx-auto gap-[30px] justify-evenly">
                {
                    data.map((item) => {
                        return <BlogItems data={item} />
                    })
                }
            </div>
        </div>
    )
};

export default Blog;